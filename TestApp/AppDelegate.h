//
//  AppDelegate.h
//  TestApp
//
//  Created by roy orpiano on 03/07/2017.
//  Copyright © 2017 raorpiano. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

- (NSManagedObjectContext *)mainManagedObjectContext;
- (void)saveContext:(BOOL)wait;


@end

