//
//  Person+CoreDataProperties.m
//  TestApp
//
//  Created by roy orpiano on 03/07/2017.
//  Copyright © 2017 raorpiano. All rights reserved.
//

#import "Person+CoreDataProperties.h"

@implementation Person (CoreDataProperties)

+ (NSFetchRequest<Person *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"Person"];
}

@dynamic name;
@dynamic address;

@end
